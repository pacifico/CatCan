 
import can
import struct
import CalibCalc as cc

a=cc.CalibCalc()

bus = can.interface.Bus(channel = "can1", bustype = "socketcan_native")

def rawTempToC(adcCount):
	fValue = float(adcCount)/float(2**24) * 5.
	tValue = 12.03*(fValue**5)-72.826*(fValue**4) + 169.91*(fValue**3) -181.65*(fValue**2)+102.63*fValue-15.548
	return tValue







msg = can.Message(arbitration_id = 0x42, data=[0, 0, 0])
bus.send(msg)
ret = bus.recv(5)
nOfBoards = int(ret.data[0])
listOfIds = []
for i in range(nOfBoards):
	#asking for the ID
	msg = can.Message(arbitration_id = 0x42, data=[3, i, 0])
	bus.send(msg)
	ret = bus.recv(5)
	id = [int(x) for x in ret.data]
	idInv = id[::-1]
	listOfIds.append(idInv)
	
	
#start acquisition
msg = can.Message(arbitration_id = 0x42, data=[1, 0, 0])
bus.send(msg)
ret = bus.recv(5)
	
print(listOfIds)
print(ret)

for i in range(nOfBoards):
	valArray = []
	for j in range(4):
		msg = can.Message(arbitration_id = 0x42, data = [2, i, j])
		bus.send(msg)
		ret = bus.recv(5)
		val = [int(x) for x in ret.data]
		valInt = struct.unpack("<i", bytes(val[0:4]))[0]
		valArray.append(valInt)
		
		print("Board {}, sensor {} is {}".format(i, j, valInt))
		if (j == 3):
			print("Temperature is " + str(rawTempToC(valArray[3])))
	id = bytes.fromhex('e50000000d558d3a')
	print(valArray)
	calibValues = a.calvtb(bytes(listOfIds[i]), valArray[0], valArray[1], valArray[2], rawTempToC(valArray[3]))
	print(calibValues)
