#include <SPI.h>
#include <FlexCAN_T4.h>
FlexCAN_T4<CAN3, RX_SIZE_256, TX_SIZE_16> Can0;
#define MISOSTATUS 22

//What we put here doesn't matter. This code (see setup function) is already running at the lowest limit of the Teensy SPI bus speed
//which is around 200 kHz. Of course if you put a higher speed than 200 kHz you will get it (at the expense of signal integrity for long strings
//of sensors...)
#define SPISPEED 10000


union intVal
{
  int32_t value;
  uint8_t bytes[4];
};

void clearCanOutBuf(uint8_t * buf){
  for(int i = 0; i<8; i++){
    buf[i] = 0x00;
  }
}
//############################################### BEGINS CS5524 LIBRARY #####################################################
#define configCmdFlag ((uint8_t)(0b0 << 7))
#define actionCmdFlag ((uint8_t)(0b1 << 7))
#define readRegFlag ((uint8_t)(0b1 << 3))
#define writeRegFlag ((uint8_t)(0b0 << 3))
#define addrOffsetRegister 0b001
#define addrGainRegister 0b010
#define addrConfRegister 0b011
#define addrChSetups 0b101

#define cmdConversion 0b000
#define cmdSelfOffsetCalib 0b001
#define cmdSelfGainCalib 0b010
#define cmdSysOffsetCalib 0b101
#define cmdSysGainCalib 0b110

#define chopFreq256 0b00
#define chopFreq4k 0b01
#define chopFreq16k 0b10
#define chopFreq1k 0b11

#define sps15 0b000
#define sps30 0b001
#define sps62 0b010
#define sps85 0b011
#define sps101 0b100
#define sps1p8 0b101
#define sps3p8 0b110
#define sps7p5 0b111

#define gain100mV 0b000
#define gain55mV 0b001
#define gain25mV 0b010
#define gain1V 0b011
#define gain5V 0b100
#define gain2p5V 0b101

#define unipolar 0b1
#define bipolar 0b0

class CS5524 {

  private:
    //The configuration register is 24 bit long. However bit shifting in a 8 bit uint8_t array is not a good idea, so we use a 32 bit
    //register instead
    uint32_t _confReg;
    //Channel setup registers are 12 bit long (and there are 8 of them).
    //However handling them like 12 bit registers is a mess, so we store each in a 16 bit register

    uint16_t _chSetupReg[8];


    uint32_t _clearBit32(uint32_t regValue, uint8_t startBit, uint8_t endBit) {
      //Clears the bits from startBit (lsb) to endBit (msb). if startBit == endBit, clears one single bit
      //This method should work also for smaller sized registers, as long as you cast the return value correctly
      for (uint8_t i = startBit; i <= endBit; i++) {
        regValue &= ~((uint32_t)(1 << i));
      }
      return regValue;
    }

    uint32_t SPIsend32(uint32_t cmdString){

      uint8_t buf[4];
      uint32_t ret;
      memcpy(buf, &cmdString, 4);
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      for(int i = 3; i>=0; i--){
        buf[i] = SPI.transfer(buf[i]);
      }
      SPI.endTransaction();
      memcpy(&ret, buf, 4);
      
      return ret;
    }

  public:
  
    uint8_t getRegDepth(){
      uint32_t rDepth = _confReg >> 12;
      rDepth = rDepth & (uint32_t)0b1111;
      return (uint8_t)rDepth + 1;     
    }
    
    void writeConfigurationRegister(void){
      uint32_t cmdString = 0;
      cmdString = ((uint32_t)(configCmdFlag | writeRegFlag |  addrConfRegister))<< 24;
      cmdString |= ((uint32_t)_confReg & 0x00FFFFFF);
      uint8_t buf[4];
      memcpy(buf, &cmdString, 4);
      SPIsend32(cmdString);
    }

    void writeGainRegister(uint8_t channelNo, uint32_t regContent){
      uint32_t cmdString = 0;
      channelNo = channelNo << 4;
      cmdString = ((uint32_t)(configCmdFlag | writeRegFlag |  addrGainRegister | channelNo));
      cmdString = cmdString << 24;
      cmdString |= ((uint32_t)regContent & 0x00FFFFFF);
      uint8_t buf[4];
      memcpy(buf, &cmdString, 4);
      SPIsend32(cmdString);
    }

    uint32_t getSysGainCalibration(uint8_t channelNo){
      uint32_t cmdString = 0;
      channelNo = channelNo << 4;
      cmdString = ((uint32_t)(configCmdFlag | readRegFlag |  addrGainRegister | channelNo))<< 24;
      uint8_t buf[4];
      memcpy(buf, &cmdString, 4);
      return (SPIsend32(cmdString) & 0x00FFFFFF);
    }

    uint32_t getSelfOffsetCalibration(uint8_t channelNo){
      uint32_t cmdString = 0;
      channelNo = channelNo << 4;
      cmdString = ((uint32_t)(configCmdFlag | readRegFlag |  addrOffsetRegister | channelNo))<< 24;
      uint8_t buf[4];
      memcpy(buf, &cmdString, 4);
      return (SPIsend32(cmdString) & 0x00FFFFFF);
    }
    
    void selfOffsetCalibration(uint8_t setupNo){
      setupNo = setupNo << 3;
      uint8_t cmdString = ((uint8_t)(actionCmdFlag | setupNo |  cmdSelfOffsetCalib));
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);
      int MISOStatus;
      for (int i = 0; i < 10000; i++) {
        MISOStatus = digitalRead(MISOSTATUS);
        if (MISOStatus == 0) break;
        delay(1);
      }
      if (MISOStatus != 0){
        Serial.print("Readout timeout during self offset calibration \n");
      }
      SPI.endTransaction();
    }

    void sysOffsetCalibration(uint8_t setupNo){
      setupNo = setupNo << 3;
      uint8_t cmdString = ((uint8_t)(actionCmdFlag | setupNo |  cmdSysOffsetCalib));
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);
      int MISOStatus;
      for (int i = 0; i < 10000; i++) {
        //Serial.println(i);
        MISOStatus = digitalRead(MISOSTATUS);
        if (MISOStatus == 0) break;
        delay(1);
      }
      if (MISOStatus != 0){
        Serial.print("Readout timeout during system offset calibration \n");
      }
      SPI.endTransaction();
    }

    void sysGainCalibration(uint8_t setupNo){
      setupNo = setupNo << 3;
      uint8_t cmdString = ((uint8_t)(actionCmdFlag | setupNo |  cmdSysGainCalib));
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);
      int MISOStatus;
      for (int i = 0; i < 10000; i++) {
        MISOStatus = digitalRead(MISOSTATUS);
        if (MISOStatus == 0) break;
        delay(1);
      }
      if (MISOStatus != 0){
        Serial.print("Readout timeout during system gain calibration \n");
      }
      SPI.endTransaction();
    }

    uint32_t readConfigurationRegister(void){
      uint32_t cmdString = 0;
      cmdString = ((uint32_t)(configCmdFlag | readRegFlag |  addrConfRegister))<< 24;
      cmdString |= (uint8_t)_confReg;

      uint32_t ret = SPIsend32(cmdString);
      return ret;
    }

    void writeChSetupRegisters(){
      uint8_t regDepth = getRegDepth();
      //Now we have the depth as set in the configuration register.

      //We have to fetch the required number of channel setup registers and pack them together.

      uint8_t srBufSize = regDepth/2*3;
      uint8_t srBuf[12];
      for(uint8_t i = 0; i<srBufSize; i++){
        srBuf[i] = 0x00;
      }
      uint8_t bbuf;
      uint16_t ibuf;
      for(uint8_t i = 0; i<regDepth; i+=2){
        //We loop on pairs of registers.
        // _chSetupReg are little endians, which means first byte is lowest significance
        ibuf = _chSetupReg[i];
        ibuf = ibuf << 4;
        srBuf[i/2*3] = (((uint8_t *)&ibuf)[1]); //first 8 MSB of the first setup of the pair
        srBuf[i/2*3+1] = (((uint8_t *)&ibuf)[0]); //remaining 4 LSB, already left padded (see 2 lines above)
        ibuf = _chSetupReg[i+1];
        bbuf = ((uint8_t *)&ibuf)[1]; //4 MSB of the second setup of the pair
        srBuf[i/2*3+1] |= bbuf;
        srBuf[i/2*3+2] = ((uint8_t *)&ibuf)[0]; //8 LSB of the second setup of the pair 
      }

      uint8_t cmdString = 0;
      cmdString = configCmdFlag | writeRegFlag |  addrChSetups;
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);
      for(uint8_t i = 0; i<srBufSize; i++){
        SPI.transfer(srBuf[i]);
      }
      SPI.endTransaction();
    }

  
    void setChSetupRegister (uint8_t regNo, uint8_t phyChannel, uint8_t latchPins, uint8_t wordRate, uint8_t gain, uint8_t polarity){
      uint16_t newRegValue = 0;
      newRegValue |= latchPins << 10;
      newRegValue |= phyChannel << 7;
      newRegValue |= wordRate << 4;
      newRegValue |= gain << 1;
      newRegValue |= polarity;
      
      _chSetupReg[regNo] = newRegValue;

    }
    

    CS5524() {
      for(uint8_t i=0; i<8; i++) _chSetupReg[i] = 0;
      
    }

    void begin(void){
      //We assume power just went on. The IC takes some time to start the oscillator, 500 ms according to datasheet
      //we play it safe and go for a 1000 ms wait

      delay(1000);

      //Writing to SPI a predefined string for placing the IC in command mode
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));

      for (int i=0; i<15; i++){
        SPI.transfer(0xFF);
      }
      SPI.transfer(0b11111110);
      SPI.endTransaction();

      //IC is in command mode.

      //now we perform a reset

      systemResetSelect(true);

      writeConfigurationRegister();
      delay(10);
      systemResetSelect(false);
      writeConfigurationRegister();
      delay(10);
      uint32_t crv = readConfigurationRegister();   
    }


    void chopFreqSelect(uint32_t freq) {
      _confReg = _clearBit32(_confReg, 20, 21);
      _confReg |= (freq << 20);
    }

    void multipleConversionsSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 18, 18);
       _confReg |= (bitVal << 18);
    }

    void loopSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 17, 17);
       _confReg |= (bitVal << 17);
    }


    void readConvertSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 16, 16);
       _confReg |= (bitVal << 16);
    }

    void depthPointerSelect(uint8_t nOfReg){
      //mind that there is no check that nOfReg is less than 8 (should be)
      //but then again this is an IC library
      
      uint32_t bitVal = nOfReg - 1; //(0 corresponds to 1 register)
      _confReg = _clearBit32(_confReg, 12, 15);
      _confReg |= (bitVal << 12);
    }
    
    void powerSaveSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 11, 11);
       _confReg |= (bitVal << 11);
    }

    void pumpDisableSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 10, 10);
       _confReg |= (bitVal << 10);
    }

    void powerSaveEnable(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 9, 9);
       _confReg |= (bitVal << 9);
    }
    

    void lowPowerModeSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 8, 8);
       _confReg |= (bitVal << 8);
    }

    void systemResetSelect(bool flag) {
      uint32_t bitVal = 0;
      if(flag) bitVal = 0b1;
       _confReg = _clearBit32(_confReg, 7, 7);
       _confReg |= (bitVal << 7);
    }

    void noWaitConvertSC(uint8_t setupNo){
      setupNo = setupNo << 3;
      uint8_t cmdString = ((uint8_t)(actionCmdFlag | setupNo |  cmdConversion));
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);
      SPI.endTransaction();
    }
    
  intVal getAsyncData(void){

    int MISOStatus;
    for (int i = 0; i < 10000; i++) {
      MISOStatus = digitalRead(MISOSTATUS);
      if (MISOStatus == 0) break;
      delay(1);
    } 
    //Clear MISO (see manual)
    SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
    SPI.transfer(0x00);
    //Now transferring the actual data:
    uint8_t buf[3];
    for(uint8_t i = 0; i<3; i++) buf[i] = 0x00;
    SPI.transfer(buf, 3);
    intVal retVal;
    retVal.bytes[3] = 0x00;
    retVal.bytes[2] = buf[0];
    retVal.bytes[1] = buf[1];
    retVal.bytes[0] = buf[2];

    if (retVal.value & 0x00800000)
      retVal.value |= 0xFF000000;    
    SPI.endTransaction();
    return retVal;
	}


    intVal dataConvertSC(uint8_t setupNo){
      setupNo = setupNo << 3;
      uint8_t cmdString = ((uint8_t)(actionCmdFlag | setupNo |  cmdConversion));
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);
      int MISOStatus;
      for (int i = 0; i < 10000; i++) {
        MISOStatus = digitalRead(MISOSTATUS);
        if (MISOStatus == 0) break;
        delay(1);
      } 
      //Clear MISO (see manual)
      SPI.transfer(0x00);
      //Now transferring the actual data:
      uint8_t buf[3];
      for(uint8_t i = 0; i<3; i++) buf[i] = 0x00;
      SPI.transfer(buf, 3);
      intVal retVal;
      retVal.bytes[3] = 0x00;
      retVal.bytes[2] = buf[0];
      retVal.bytes[1] = buf[1];
      retVal.bytes[0] = buf[2];

      if (retVal.value & 0x00800000)
        retVal.value |= 0xFF000000;    
      SPI.endTransaction();
      return retVal;
    }

    uint8_t dataConvertMC(uint8_t * dataBuffer){
      //The function issues a dataConvert command, waits for the acquisition ready flag (MISO up)
      //and then puts the data in dataBuffer. The return value is the length of the buffer.

      uint8_t bufferSize = getRegDepth() * 3;

      uint8_t cmdString = ((uint8_t)(actionCmdFlag |  cmdConversion));
      SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
      SPI.transfer(cmdString);

      //wait for conversion to complete:

      uint8_t MISOStatus;
      for (int i = 0; i < 10000; i++) {
        MISOStatus = digitalRead(MISOSTATUS);
        delay(1);
        if (MISOStatus == 0) break;
      }

      //Clear MISO (see manual)
      SPI.transfer(0x00);
      //Now transferring the actual data:
      uint8_t buf[bufferSize];
      for(uint8_t i = 0; i<bufferSize; i++) buf[i] = 0x00;
      SPI.transfer(buf, 24);

      memcpy(dataBuffer, buf, 24);
      SPI.endTransaction();
      return bufferSize;
    }
};

//############################################### ENDS CS5524 LIBRARY #####################################################


//############################################### BEGINS DS2413 LIBRARY #####################################################

#include <OneWire.h>

#define DS2413_FAMILY_ID    0x3A
#define DS2413_ACCESS_READ  0xF5
#define DS2413_ACCESS_WRITE 0x5A
#define DS2413_ACK_SUCCESS  0xAA
#define DS2413_ACK_ERROR    0xFF

class DS2413{
  public:
  uint8_t address[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
  OneWire * oneWireDevice;
  uint8_t _spiChannel;
  uint8_t _tsuPort;

  DS2413(){
    //Do nothing.
  }

  uint8_t * getAddress(void){
    return address;
  }
  
  void begin(uint8_t spiChannel, uint8_t tsuPort, OneWire * owr, uint8_t * addr){
    oneWireDevice = owr;
    _spiChannel = spiChannel;
    _tsuPort = tsuPort;
    memcpy(address, addr, 8);
  }


  static void getDeviceList(uint8_t addressList[][8], OneWire * owr){
    uint8_t addr[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

    uint8_t cnt=0;
    owr->reset_search();
    delay(250);
    while(1){
      if (!owr->search(addr))
      {
        owr->reset_search();
        break;
      }
    
      /* Check the CRC in the device address */
      if (OneWire::crc8(addr, 7) != addr[7])
      {
        Serial.println(F("Invalid CRC!"));
        continue;
      }
    
      /* Make sure we have a DS2413 */
      if (addr[0] != DS2413_FAMILY_ID)
      {
        Serial.println(F(" is not a DS2413!"));
        
        continue;
      }
      memcpy(addressList[cnt], addr, 8);
      cnt++;
    }
    
  }  

  static uint8_t getDeviceCount(OneWire * owr){
    uint8_t addr[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
   
    uint8_t cnt=0;
    owr->reset_search();
    delay(250);
    while(1){
      if (!owr->search(addr))
      {
        //printBytes(address, 8);
        //Serial.println(F("No more devices found on the bus!"));
        owr->reset_search();
        break;
      }
    
      /* Check the CRC in the device address */
      if (OneWire::crc8(addr, 7) != addr[7])
      {
        Serial.println(F("Invalid CRC!"));
        continue;
      }
    
      /* Make sure we have a DS2413 */
      if (addr[0] != DS2413_FAMILY_ID)
      {
        //printBytes(address, 8);
        Serial.println(F(" is not a DS2413!"));
        
        continue;
      }
    
      //Serial.print(F("Found a DS2413: "));
      //printBytes(address, 8);
      //Serial.println(F(""));
      cnt++;
    }

    return cnt;
    
  }

  byte read(void) {
    bool ok = false;
    uint8_t results;
  
    oneWireDevice->reset();
    oneWireDevice->select(address);
    oneWireDevice->write(DS2413_ACCESS_READ);
  
    results = oneWireDevice->read();                 /* Get the register results   */
    ok = (!results & 0x0F) == (results >> 4); /* Compare nibbles            */
    results &= 0x0F;                          /* Clear inverted values      */
  
    oneWireDevice->reset();
  
    // return ok ? results : -1;
    return results;
  }

  bool pullDown(){
    digitalWrite(_spiChannel, LOW);
    digitalWrite(_tsuPort, HIGH);
    return write(0x00);
  }


  bool pullUp(){
    digitalWrite(_spiChannel, HIGH);
    digitalWrite(_tsuPort, HIGH);
    return write(0x03);
  }
  
  bool write(uint8_t state)
  {
    //Serial.println("Write called");
    uint8_t ack = 0;
  
    /* Top six bits must '1' */
    state |= 0xFC;
  
    oneWireDevice->reset();
    oneWireDevice->select(address);
    oneWireDevice->write(DS2413_ACCESS_WRITE);
    oneWireDevice->write(state);
    oneWireDevice->write(~state);                    /* Invert data and resend     */
    ack = oneWireDevice->read();                     /* 0xAA=success, 0xFF=failure */
    if (ack == DS2413_ACK_SUCCESS)
    {
      oneWireDevice->read();                          /* Read the status byte      */
    }
    oneWireDevice->reset();
  
    return (ack == DS2413_ACK_SUCCESS ? true : false);
  }
  
  static void printBytes(uint8_t * addr, bool newline = 0){
    for (uint8_t i = 0; i < 8; i++)
    {
      Serial.print(addr[i] >> 4, HEX);
      Serial.print(addr[i] & 0x0f, HEX);
      Serial.print(" ");
    }
    if (newline)
    {
      Serial.println();
    }
  }
};
//############################################### ENDS DS2413 LIBRARY #####################################################


class bFieldBoard : public CS5524, public DS2413{
  public:
  bFieldBoard(){
    CS5524();
    DS2413();
  }
  virtual void begin(uint8_t spiChannel, uint8_t tsuPort, OneWire * owr, uint8_t * addr){
    DS2413::begin(spiChannel, tsuPort, owr, addr);
    //pullUp();
  }
  void initADC(){
    CS5524::begin();
  }
};


const int32_t MODULO = 1L << 24;
const int32_t MAX_VALUE = ((1L << 23) - 1);

int32_t transform(int32_t value) {
  if (value > MAX_VALUE) {
    value -= MODULO;
  }
  return value;
}

bool newCanMessage = false;
bool flagStartAcquisition = false;
bool flagReturnCount = false;
bool flagGetAdcValue;
bool flagGetBoardId;
uint8_t bNumber, sNumber;
int totalBoardsCount = 0;


void canSniff(const CAN_message_t &msg) {
  //We expect one message from host 0x42 and it should be three bytes long.
  if(msg.id !=0x42 || msg.len!=3){
    return;
  }
  
  newCanMessage = true;

  //Byte 0 is Operation
  switch(msg.buf[0]){
    case 0:
      flagReturnCount = true;
      break;
    case 1:
      flagStartAcquisition = true;
      break;
    case 2:
      bNumber = msg.buf[1];
      sNumber = msg.buf[2];
      Serial.print("Received ADC get command, b, s = ");
      Serial.print(bNumber);
      Serial.print(", ");
      Serial.println(sNumber);
      if(bNumber>totalBoardsCount - 1 || sNumber > 3) return;
      flagGetAdcValue=true;
      break;
    case 3:
      bNumber = msg.buf[1];
      if(bNumber>totalBoardsCount - 1) return;
      flagGetBoardId = true;
  }
}

const int identPin = 28;

OneWire * oneWireRefs[4];
bFieldBoard boardsArray[128];
uint8_t boardsCount[4];
uint8_t spiEnablePorts[] = {21, 20, 19, 18};

uint8_t oneWirePorts[] = {29, 28, 27, 26};
uint8_t tsuPorts[] = {6, 4, 2, 0};


intVal * * adcValuesMatrix;

void setup() {

  Can0.begin();
  Can0.setBaudRate(500000);
  Can0.setMaxMB(16);
  Can0.enableFIFO();
  Can0.enableFIFOInterrupt();
  Can0.onReceive(canSniff);
  Can0.mailboxStatus();

  SPI.begin();
  CCM_CBCMR = (CCM_CBCMR & ~(CCM_CBCMR_LPSPI_PODF_MASK | CCM_CBCMR_LPSPI_CLK_SEL_MASK)) |
    CCM_CBCMR_LPSPI_PODF(7) | CCM_CBCMR_LPSPI_CLK_SEL(3);
  
  /*
   * A more orthodox approach than doing a beginTransaction at init time
   * would be to look for all instances of SPI.transfer() and enclose 
   * them between a beginTransaction and endTransaction. However, given
   * that the CS5524 has a very creative approach towards SS, this is a 
   * plain overkill 
   * */
  
  //SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
  
  
  pinMode(0, OUTPUT);     //TS4
  digitalWrite(0, HIGH);  
  pinMode(1, OUTPUT);     //SPU4
  digitalWrite(1, HIGH);   
  pinMode(2, OUTPUT);     //TS3
  digitalWrite(2, HIGH);
  pinMode(3, OUTPUT);     //SPU3
  digitalWrite(3, HIGH);
  pinMode(4, OUTPUT);     //TS2
  digitalWrite(4, HIGH);
  pinMode(5, OUTPUT);     //SPU2
  digitalWrite(5, HIGH);
  pinMode(6, OUTPUT);     //TS1
  digitalWrite(6, HIGH);
  pinMode(7, OUTPUT);     //SPU1
  digitalWrite(7, LOW);
  pinMode(18, OUTPUT);    //SPI4
  digitalWrite(18, HIGH);
  pinMode(19, OUTPUT);    //SPI3
  digitalWrite(19, HIGH);
  pinMode(20, OUTPUT);    //SPI2
  digitalWrite(20, HIGH);
  pinMode(21, OUTPUT);    //SPI1
  digitalWrite(21, HIGH);
  pinMode(identPin, OUTPUT);
  digitalWrite(identPin, HIGH);

  pinMode(MISOSTATUS, INPUT);

  pinMode(10, OUTPUT);
  digitalWrite(10, HIGH);

  Serial.begin(57600);
  Serial.println("This is the new version");
  //First thing to do, is look for the devices on the one-wire bus
  //We do so

  
  for(int i=0; i<4; i++){
      digitalWrite(tsuPorts[i], HIGH);
      oneWireRefs[i] = new OneWire(oneWirePorts[i]);
      boardsCount[i] = bFieldBoard::getDeviceCount(oneWireRefs[i]);
      Serial.print("Boards count is ");
      Serial.println(boardsCount[i]);
      uint8_t deviceAddressList[boardsCount[i]][8];
      bFieldBoard::getDeviceList(deviceAddressList, oneWireRefs[i]);
      digitalWrite(tsuPorts[i], LOW);
      for(uint8_t j = 0; j<boardsCount[i]; j++){
        boardsArray[totalBoardsCount].begin(spiEnablePorts[i], tsuPorts[i], oneWireRefs[i], deviceAddressList[j]);
        boardsArray[totalBoardsCount].pullUp();
        totalBoardsCount++;
      }
  }

  adcValuesMatrix = (intVal * *) malloc(sizeof(intVal *) * totalBoardsCount);
  for(int i=0; i<totalBoardsCount; i++){
    adcValuesMatrix[i] = (intVal *) malloc(sizeof(intVal) * 4);
  }

  Serial.print("Found ");
  Serial.print(totalBoardsCount);
  Serial.println(" boards");


  /* The following lines are for dynamic boardArray allocation. Doesn't
   * work well for an Arduino, might on a Teensy, but if memory is not an issue
   * no reason to go there.... */
  //boardsArray = (bFieldBoard * *)malloc(sizeof(bFieldBoard *) * boardsCount);
  //Serial.print("boardsArray pointer is ");
  //Serial.println((uint32_t)boardsArray, HEX);


  //Now all boards are "created" and the SS are pulled up.
  //Now, one by one, we "init" the ADC (CS5524 begin) and
  //we further configure them
  for(uint8_t i = 0; i<totalBoardsCount; i++){
    boardsArray[i].pullDown();
    delay(100);
    boardsArray[i].initADC();
    delay(100);
    boardsArray[i].depthPointerSelect(8);
    boardsArray[i].writeConfigurationRegister();
    boardsArray[i].setChSetupRegister (0, 0, 0b00, sps1p8, gain100mV, bipolar);
    boardsArray[i].setChSetupRegister (1, 1, 0b00, sps1p8, gain100mV, bipolar);
    boardsArray[i].setChSetupRegister (2, 2, 0b00, sps1p8, gain100mV, bipolar);
    boardsArray[i].setChSetupRegister (3, 3, 0b00, sps1p8, gain100mV, bipolar); // current monitor input
    boardsArray[i].setChSetupRegister (4, 3, 0b01, sps1p8, gain2p5V, bipolar); //NTC
    boardsArray[i].setChSetupRegister (5, 3, 0b10, sps1p8, gain2p5V, bipolar); //0C calibration input
    boardsArray[i].setChSetupRegister (6, 3, 0b11, sps1p8, gain2p5V, bipolar); //100C calibration input
    //test.setChSetupRegister (7, 3, 0b11, sps15, gain2p5V, bipolar); //Dummy
    
    boardsArray[i].writeChSetupRegisters();
    
    Serial.print("Initiating self calibration for board: ");
    bFieldBoard::printBytes(boardsArray[i].getAddress(), true);

    for(uint8_t j = 0; j<4; j++){
      boardsArray[i].selfOffsetCalibration(j);
        
      Serial.print("Self offset calibration is ");
      Serial.print(boardsArray[i].getSelfOffsetCalibration(j), HEX);
      Serial.print("\n");
    
    }

    boardsArray[i].sysGainCalibration(3);

    uint32_t gainCalib = boardsArray[i].getSysGainCalibration(3);

    for(uint8_t j = 0; j < 3; j++){
      boardsArray[i].writeGainRegister(j, gainCalib);
    }

    boardsArray[i].sysOffsetCalibration(5);
    boardsArray[i].sysGainCalibration(6);

    boardsArray[i].multipleConversionsSelect(false);
    boardsArray[i].writeConfigurationRegister();
    boardsArray[i].setChSetupRegister (0, 0, 0b00, sps15, gain100mV, bipolar);
    boardsArray[i].setChSetupRegister (1, 1, 0b00, sps15, gain100mV, bipolar);
    boardsArray[i].setChSetupRegister (2, 2, 0b00, sps15, gain100mV, bipolar);
    boardsArray[i].setChSetupRegister (4, 3, 0b01, sps15, gain2p5V, bipolar); //NTC
    boardsArray[i].writeChSetupRegisters();
    
    delay(1);
    boardsArray[i].pullUp();
  }

  CAN_message_t msg;
   msg.id = 0x33;
   msg.len = 1;
   msg.buf[0] = 'R';
   Can0.write(msg);
}

uint8_t channelsList[] = {0, 1, 2, 4};

void loop() {
  CAN_message_t msg;

  if(flagReturnCount){
    flagReturnCount = false;
    msg.id = 0x33;
    msg.len = 1;
    msg.buf[0] = totalBoardsCount;
    Can0.write(msg);
  }

  if(flagStartAcquisition){
    flagStartAcquisition = false;
    intVal adcValue;
    Serial.println(millis());
    
    for(int j=0; j<4; j++){
		for(int i=0; i<totalBoardsCount; i++){
			boardsArray[i].pullDown();
			delayMicroseconds(100);
			boardsArray[i].noWaitConvertSC(channelsList[j]);
			delayMicroseconds(100);
			boardsArray[i].pullUp();
		}
		for(int i=0; i<totalBoardsCount; i++){
			boardsArray[i].pullDown();
			delayMicroseconds(100);
			adcValuesMatrix[i][j] = boardsArray[i].getAsyncData();
			delayMicroseconds(100);
			boardsArray[i].pullUp();
		}
	}
		
    
    
    
    /*
     * Old code: wait for each acquisition
     * 
    for(int i=0; i<totalBoardsCount; i++){
      boardsArray[i].pullDown();
      for(int j=0; j<4; j++){
        adcValuesMatrix[i][j] = boardsArray[i].dataConvertSC(channelsList[j]);
      }
      boardsArray[i].pullUp();                   
    }
    */
    
    clearCanOutBuf(msg.buf);
    msg.id=0x33;
    msg.len = 8;
    msg.buf[0] = 0xFF;
    Can0.write(msg);    
  }

  if(flagGetAdcValue){
    flagGetAdcValue = false;
    clearCanOutBuf(msg.buf);
    msg.id=0x33;
    msg.len = 8;
    memcpy(msg.buf, &adcValuesMatrix[bNumber][sNumber], 4);
    msg.buf[7] = sNumber;
    msg.buf[6] = bNumber;
    Serial.println("============");
    for(int i=0; i<8; i++){
      Serial.print(msg.buf[i], HEX);
    }
    Serial.println("");
    Can0.write(msg);      
  }

  if(flagGetBoardId){
    flagGetBoardId = false;
    clearCanOutBuf(msg.buf);
    uint8_t * tmp;
    tmp = boardsArray[bNumber].getAddress();
    msg.id=0x33;
    msg.len = 8;
    memcpy(msg.buf, tmp, 8);
    Can0.write(msg);  
  }
    
  delay(100);
    
}
  
